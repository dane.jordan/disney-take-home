import '../styles/index.scss';
import 'whatwg-fetch';

const API_URL =
  'http://statsapi.mlb.com/api/v1/schedule?hydrate=game(content(editorial(recap))),decisions&date=2018-06-10&sportId=1';

const HEADLINE_INDEX = 0;
const THUMBNAIL_INDEX = 1;
const DESC_INDEX = 2;

let selectedIndex = 0;
let largestIndex = 0;

const onLoad = () => {
  document.addEventListener('keydown', changeSelectedIndex);
  document.addEventListener(
    'webkitfullscreenchange',
    changeSelectedIndex,
    false
  );
  document.addEventListener('mozfullscreenchange', changeSelectedIndex, false);
  document.addEventListener('fullscreenchange', changeSelectedIndex, false);
  document.addEventListener('MSFullscreenChange', changeSelectedIndex, false);
  return window
    .fetch(API_URL)
    .then(res => res.json())
    .then(populateDom);
};

const populateDom = rawData => {
  populateContainer(extractData(rawData));
};

const extractData = rawJson => rawJson.dates[0].games;

const populateContainer = games => {
  largestIndex = games.length - 1;
  games.forEach(renderGame);
};

const renderGame = game => {
  let headline = renderHeadline(game);
  let thumbnail = renderThumbnail();
  let desc = renderDescription(game);
  let frame = renderFrame();

  frame.appendChild(headline);
  frame.appendChild(thumbnail);
  frame.appendChild(desc);

  document.getElementById('cont').appendChild(frame);
};

const renderHeadline = game => {
  let headlineCont = document.createElement('h4');
  headlineCont.classList.add('hidden');
  headlineCont.classList.add('headline-cont');

  let headlineTop = document.createElement('h4');
  headlineTop.innerText = `${game.teams.away.team.name}`;
  headlineTop.classList.add('headline');

  let headlineMid = document.createElement('h4');
  headlineMid.innerText = 'vs.';
  headlineMid.classList.add('headline');

  let headlineBot = document.createElement('h4');
  headlineBot.innerText = `${game.teams.home.team.name}`;
  headlineBot.classList.add('headline');

  headlineCont.appendChild(headlineTop);
  headlineCont.appendChild(headlineMid);
  headlineCont.appendChild(headlineBot);
  return headlineCont;
};

const renderThumbnail = () => {
  let thumbnail = document.createElement('div');
  thumbnail.classList.add('thumbnail');
  return thumbnail;
};

const renderDescription = game => {
  let desc = document.createElement('h5');
  desc.innerText = `${game.venue.name}`;
  desc.classList.add('hidden');
  desc.classList.add('desc');
  desc.classList.add('center');
  return desc;
};

const renderFrame = () => {
  let frame = document.createElement('div');
  frame.classList.add('frame');
  return frame;
};

const processInputChange = prevIndex => {
  hidePrevChild(prevIndex);
  highlightNextChild();
};

const highlightNextChild = () => {
  let nextChild = document.getElementById('cont').childNodes[selectedIndex];
  nextChild.classList.add('frame-selected');
  nextChild.childNodes[THUMBNAIL_INDEX].classList.add('thumbnail-selected');
  nextChild.childNodes[HEADLINE_INDEX].classList.remove('hidden');
  nextChild.childNodes[DESC_INDEX].classList.remove('hidden');
};

const hidePrevChild = prevIndex => {
  let prevChild = document.getElementById('cont').childNodes[prevIndex];
  prevChild.classList.remove('frame-selected');
  prevChild.childNodes[THUMBNAIL_INDEX].classList.remove('thumbnail-selected');
  prevChild.childNodes[THUMBNAIL_INDEX].classList.remove('thumbnail-selected');
  prevChild.childNodes[THUMBNAIL_INDEX].classList.add('thumbnail');
  prevChild.childNodes[HEADLINE_INDEX].classList.add('hidden');
  prevChild.childNodes[DESC_INDEX].classList.add('hidden');
};

const didIncrementIndex = () => {
  if (selectedIndex + 1 <= largestIndex) {
    selectedIndex += 1;
    return true;
  }
  return false;
};

const didDecrementIndex = () => {
  if (selectedIndex - 1 >= 0) {
    selectedIndex -= 1;
    return true;
  }
  return false;
};

const changeSelectedIndex = e => {
  switch (e.code) {
    case 'ArrowUp':
    case 'ArrowLeft':
    case 'PageUp': {
      let prevIndex = selectedIndex;
      if (didDecrementIndex()) {
        processInputChange(prevIndex);
      }
      break;
    }
    case 'ArrowDown':
    case 'ArrowRight':
    case 'PageDown':
      {
        let prevIndex = selectedIndex;
        if (didIncrementIndex()) {
          processInputChange(prevIndex);
        }
      }
      break;
    default:
      break;
  }
};

onLoad().then(() => processInputChange(selectedIndex));
